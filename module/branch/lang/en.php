<?php
$lang->branch->common = 'Branch';
$lang->branch->manage = 'Branch manage';
$lang->branch->delete = 'Branch delete';

$lang->branch->all = 'All';

$lang->branch->confirmDelete = 'Branch deletion, will affect the branch of the demand, module, plan, release, Bug, testcase and so on, please consider carefully. Are you sure delete the branch?';
