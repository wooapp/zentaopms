<?php
$lang->branch->common = '分支';
$lang->branch->manage = '分支管理';
$lang->branch->delete = '分支刪除';

$lang->branch->all = '所有';

$lang->branch->confirmDelete = '分支刪除，會影響關聯該分支的需求、模組、計劃、發佈、Bug、用例等等，請慎重考慮。是否刪除改分支？';
